#pragma once

#include <iostream>
#include <thread>
#include <chrono>
#include <ctime>
#include <sstream>

#include "rapidjson\document.h"
#include "rapidjson\prettywriter.h"
#include "rapidjson\stringbuffer.h"

namespace rj = rapidjson;

/* Initialize new JSON request */
rj::Document initJSON(int command)
{
	rj::Document document;
	document.SetObject();

	/// Must pass an allocator when the object may need to allocate memory
	auto& allocator = document.GetAllocator();

	/// Get id current thread
	std::stringstream ss;
	ss << std::this_thread::get_id();
	uint64_t threadId = std::stoull(ss.str());

	/// Get current time
	time_t ttime = time(nullptr);
	tm* timeNow = new tm();
	localtime_s(timeNow, &ttime);

	/// Clear sstream
	ss.str(std::string());
	ss.clear();

	/// Write current time 
	ss << timeNow->tm_hour << ":"
		<< timeNow->tm_min << ":"
		<< timeNow->tm_sec;

	delete timeNow;

	rj::Value time;
	time.SetString(ss.str().c_str(), allocator);

	/// Add to JSON document our fields
	document.AddMember("command", command, allocator);
	document.AddMember("thread_id", threadId, allocator);
	document.AddMember("time", time, allocator);

	return document;
}

/* Convert JSON document to std::string */
std::string stringJSON(const rj::Document& document)
{
	rj::StringBuffer strbuf;
	rj::Writer <rj::StringBuffer> writer(strbuf);

	document.Accept(writer);

	return std::string(strbuf.GetString());
}