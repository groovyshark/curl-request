#include "interact.hpp"
#include <mutex>
#include <vector>
#include <future>

std::mutex mutex;
std::vector <std::future <std::string> > requests;

/* Check that object is ready */
template<typename R>
bool isReadyFuture(const std::future <R>& f)
{
	return f.wait_for(std::chrono::seconds(0)) == std::future_status::ready;
}

/* Show return values from async */
void outputFutures()
{
	while (true)
	{
		mutex.lock();
		/// Iterate over all requests and
		/// show, if future is ready
		for (auto it = requests.begin(); it != requests.end();)
		{
			if (isReadyFuture(*it))
			{
				std::cout << it->get();

				it = requests.erase(it);
			}
			else
				++it;
		}
		mutex.unlock();
	}
}

int main()
{
	/// Create new thread for output responces from Server
	std::thread out(outputFutures);
	out.detach();

	while (true)
	{	
		/// Get command from user
		int command;
		std::cin >> command;

		/// Check if command is correct
		if (command < 1 || command > 9)
		{
			std::cout << "Error: unknown command, try again..." << std::endl;
			continue;
		}

		/// Create new async thread and
		/// push result of async in vector of futures
		mutex.lock();
		requests.emplace_back(std::async(std::launch::async, newRequest, command));
		mutex.unlock();
	}

	return 0;
}