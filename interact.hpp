#pragma once
#include <curl\curl.h>
#include "json.hpp"

#define DOMAIN_NAME		"http://example.com"
#define SERVER_RESPONSE "Accepted"

namespace rj = rapidjson;

/* Server response function */
size_t serverWriteCallback(void *ptr, size_t size, size_t nmemb, std::string* s)
{
	rj::Document d;
	
	/// Check parse errors
	if (!d.Parse <0> (s->c_str()).HasParseError()) 
	{
		std::cout << "Server: request from " << d["thread_id"].GetInt()
			<< stringJSON(d) << std::endl;
	}

	/// Write response
	*s = SERVER_RESPONSE;

	return size * nmemb;
}

/* Function for new request to server */
std::string newRequest(int command)
{
	std::stringstream ss;

	/// Initialize JSON request 
	rj::Document json = initJSON(command);
	
	CURL* curl = curl_easy_init();
	if (!curl)
	{
		ss << "Client " << json["thread_id"].GetInt() << "curl not initialized" << std::endl;
		return ss.str();
	}

	/// Convert JSON object to std::string
	std::string request = stringJSON(json);
	
	curl_easy_setopt(curl, CURLOPT_URL, DOMAIN_NAME);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, serverWriteCallback);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &request);

	CURLcode res = curl_easy_perform(curl);

	/// Error by executing the request 
	if (res != CURLE_OK)
	{
		ss << "Client " << json["thread_id"].GetInt() << "curl_easy_perform() failed: "
		   << curl_easy_strerror(res) << std::endl;
		return ss.str();
	}

	curl_easy_cleanup(curl);

	/// Write response from server to string
	ss << "Client "<< json["thread_id"].GetInt() << " : response from server " 
				   << request << std::endl;
	return ss.str();
}